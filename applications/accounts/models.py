# coding:utf-8
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.validators import RegexValidator
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class User(AbstractUser):
    inn = models.CharField(
        verbose_name='ИНН', max_length=12,
        validators=[RegexValidator(
            regex='^\d{10,12}$', message="Введите правильный ИНН"
        )]
    )
    score = models.DecimalField(
        'Счет', max_digits=9, decimal_places=2, default=0
    )

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def __str__(self):
        return self.username
