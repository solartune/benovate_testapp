from django.conf.urls import url

from .views import payments
from .ajax import transaction

urlpatterns = [
    url(r'^$', payments, name='payments'),
]

urlpatterns += [
    url(r'^transaction/$', transaction, name='transaction'),

]
