# coding:utf-8
from __future__ import unicode_literals

from django.shortcuts import render

from .forms import PaymentForm


def payments(request):
    form = PaymentForm(request.POST or None)
    return render(request, 'payments/payment.html', {"form": form})
