# coding:utf-8
from __future__ import unicode_literals

from django.http import JsonResponse
from django.db.models import F

from .forms import PaymentForm
from applications.accounts.models import User


def transaction(request):
    form = PaymentForm(request.POST or None)
    if form.is_valid():
        user_id = form.cleaned_data.get('user')
        inn = form.cleaned_data.get('inn')
        score = form.cleaned_data.get('score')
        
        user_out = User.objects.get(id=user_id)
        users_in = User.objects.filter(inn=inn).exclude(id=user_id)
        count_users = users_in.count()
        parts = score / count_users

        user_out.score -= score
        user_out.save()

        users_in.update(score=F('score') + parts)

        return JsonResponse(
            {'status': 'success', "msg": "Транзакция успешно проведена"}
        )
    else:
        return JsonResponse({'status': 'error', "msg": form.errors})
