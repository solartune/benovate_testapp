# coding:utf-8
from __future__ import unicode_literals

from django import forms
from django.core.validators import RegexValidator

from django_select2.forms import ModelSelect2Widget

from applications.accounts.models import User


class PaymentForm(forms.Form):
    user = forms.IntegerField(
        widget=ModelSelect2Widget(
            attrs={'data-placeholder': 'Введите имя пользователя'},
            model=User,
            search_fields=['username__istartswith']
        )
    )
    inn = forms.CharField(
        validators=[RegexValidator(
            regex='^\d{10,12}$', message="Введите правильный ИНН"
        )]
    )
    score = forms.DecimalField(max_digits=9, decimal_places=2)

    def clean_inn(self):
        inn = self.cleaned_data.get('inn')
        user_id = self.cleaned_data.get('user')
        users = User.objects.filter(inn=inn).exclude(id=user_id)
        if users:
            return inn
        else:
            raise forms.ValidationError("Пользователи с таким ИНН не найдены")

    def clean_score(self):
        score = self.cleaned_data.get('score')
        user_id = self.cleaned_data.get('user')
        user = User.objects.get(id=user_id)
        if score < 0:
            raise forms.ValidationError("Введите положительную сумму")
        if user.score >= score:
            return score
        else:
            raise forms.ValidationError(
                "Указанная сумма превышает баланс пользователя"
            )
