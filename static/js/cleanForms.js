$(document).ready(function () {
    var paymentForm = $("#payment_form");

    $(".js-submit").on('click', function () {
        $(".error").html('');
        paymentForm.ajaxSubmit({
            success: function (data) {
                console.log(data);
                if (data.status === 'success') {
                    paymentForm.resetForm();
                    $(".success").html(data.msg);
                    setTimeout(function () {
                        $(".success").html('')
                    }, 3000);
                } else {
                    $.each(data.msg, function (name, obj) {
                        var error = $('#id_' + name).parent().next('.error');
                        error.text(obj);

                    })
                }
            }
        });
    });

});
